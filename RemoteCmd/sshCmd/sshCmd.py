import sys
import paramiko

hostname = "".join(sys.stdin.readline()).replace("\n", "")
username = "".join(sys.stdin.readline()).replace("\n", "")
password = "".join(sys.stdin.readline()).replace("\n", "")
cmd = "".join(sys.stdin.readlines())


ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname=hostname, username=username, password=password)

stdin, stdout, stderr = ssh_client.exec_command('sudo su tux')
stdin.write(cmd)
stdin.channel.shutdown_write()

for out in iter(stdout.readline, ""):
    sys.stdout.write(out)
    sys.stdout.flush()

for err in iter(stderr.readline, ""):
    sys.stderr.write(err)
    sys.stderr.flush()

exit_status = int(stdout.channel.recv_exit_status())
ssh_client.close()
exit(exit_status)



