import sublime
import sublime_plugin
import logging
import threading
import time
import os 
import json
import uuid
import pickle
import codecs
import subprocess
from subprocess import Popen, PIPE, STDOUT


PROCS = []
PLUGIN_NAME = "ExecCmdPlugin"
PANEL_NAME = "Results" 
LAST_SHOWED_CMD = None
PANEL_VIEW = None

def init_context_menu():


    context_menu = [{   "caption": "Action",
                        "id": "action",
                        "children":[
                            { "command": "remote_cmd", "args": {"type_cmd": 1 }, "caption": "Hive -e" },
                            { "command": "remote_cmd", "args": {"type_cmd": 2 }, "caption": "Show create table" },
                            { "command": "remote_cmd", "args": {"type_cmd": 3 }, "caption": "Limit 10" },
                            { "command": "remote_cmd", "args": {"type_cmd": 4 }, "caption": "Hdfs dfs -ls" },
                            { "command": "remote_cmd", "args": {"type_cmd": 5 }, "caption": "CMD" },
                            { "command": "hide_panel", "args": {"panel": "output.{}".format(PANEL_NAME) }, "caption": "Hide result panel" }]}]
 


    cache_path = os.path.join(sublime.cache_path(), PLUGIN_NAME)
    os.makedirs(cache_path, exist_ok=True)

    with open(os.path.join(cache_path, "Context.sublime-menu"), "w+") as cache:
        cache.write(json.dumps(context_menu, cache))


init_context_menu()

def thread_cmd(window, edit, thread_id, conn):

    dir_path = os.path.dirname(os.path.realpath(__file__))

    pr = Popen(["/home/tux/PycharmProjects/pythonProject1/venv/bin/python", dir_path+"/sshCmd/sshCmd.py"], 
        stdout=PIPE, stdin=PIPE, stderr=PIPE, universal_newlines=True)

    p = get_cmd(thread_id)

    pr.stdin.write(conn["hostname"]+"\n"+conn["username"]+"\n"+conn["password"]+"\n"+p[1])
    pr.stdin.close()


    for out in iter(pr.stdout.readline, ""):
        for j, p in enumerate(PROCS):
            if thread_id == p[3]:
                #new_line = "output "+str(out.replace("\n",""))+" "+str(thread_id)+"\n"
                new_line = str(out.replace("\n",""))+"\n"
                p[4] = p[4] + new_line

                if window.active_panel() == "output.{}".format(PANEL_NAME) and thread_id == LAST_SHOWED_CMD:
                    PANEL_VIEW.set_read_only(False)
                    PANEL_VIEW.run_command("insert", {"characters": new_line})
                    PANEL_VIEW.set_read_only(True)
                

    if pr.poll() != 0:
        msg_err="La command suivant:\n"
        err = ""
        for line in iter(pr.stderr.readline, ""):
            err+=line
        cmd = ""    
        for p in PROCS:
            if thread_id == p[3]:
                cmd = p[1]
        msg_err+=cmd+"\n a fini avec le status "+str(pr.poll())+"\nstderr: \n"+err

        sublime.message_dialog(msg_err)

    #view = window.new_file()
    #view.run_command("insert", {"characters": "Hello"})
    #view.set_read_only(True)
    #sublime.error_message("fin du tread")
    #window.create_output_panel("mypanel")
    status_to_end(thread_id)
    update_menu()


def get_cmd(thread_id):
    global PROCS
    for p in PROCS:
        if p[3] == thread_id:
            return p

def status_to_end(thread_id):
    for p in PROCS:
        if p[3] == thread_id:
            p[2] = "end"

def update_menu():

    main_menu = [{
        "caption": "Exe proc",
        "id": "exe_proc",
        "children":[]
    }]

    for p in reversed(PROCS):
        state_icon = "\t"
        if p[2]=="start":
            state_icon = "⚙\t"

        main_menu[0]["children"].append({"command": "show_cmd", "args": {"ident": str(p[3])}, "caption": state_icon+p[1].replace("\n","")[:50]})
    
    cache_path = os.path.join(sublime.cache_path(), PLUGIN_NAME)
    os.makedirs(cache_path, exist_ok=True)

    with open(os.path.join(cache_path, "Main.sublime-menu"), "w+") as cache:
        cache.write(json.dumps(main_menu, cache))

update_menu()

class remote_cmd(sublime_plugin.TextCommand):
    def run(self, edit, type_cmd):

        conn = {"hostname":"localhost",
                "username":"", 
                "password":""}

        if len(self.view.sel()) > 1:
            sublime.error_message("Vous devez selectioner une seul region !")
            return 


        selection = self.view.substr(self.view.sel()[0])

        if selection == "":
            sublime.error_message("Vous devez selectioner un contenu !")
            return

        cmd = ""
        if type_cmd == 1: 
            cmd = 'hive -e "{selection}"'
        elif type_cmd == 2:
            cmd = 'hive --showHeader=false --outputformat=tsv2 -e "show create table {selection}"'
        elif type_cmd == 3:
            cmd = 'hive -e "{select * from {selection} limit 10}"'
        elif type_cmd == 4:
            cmd = 'hdfs dfs -ls {selection}'
        elif type_cmd == 5:
            cmd = '{selection}'
        else:
            sublime.error_message("Type commande non reconnu !")
            return

        cmd = cmd.format(selection=selection)

        global PROCS

        thread_id = uuid.uuid1()
        x = threading.Thread(target=thread_cmd, args=(self.view.window(),edit, thread_id, conn))
        x.start()
        #x.join() 
        PROCS.append([x, cmd, "start", thread_id, ""]) 
        update_menu() 

        self.view.window().run_command("show_cmd", {"ident": str(thread_id)})


        #sublime.message_dialog(str(p))
        #sublime.ok_cancel_dialog("msg", ok_title='titre', title='titre 2')
        #sublime.yes_no_cancel_dialog("msg", yes_title='yes_title', no_title='no_title', title='title')

class show_cmd(sublime_plugin.TextCommand):
    def run(self, edit, ident):

        global PANEL_VIEW
        global LAST_SHOWED_CMD
        PANEL_VIEW = self.view.window().create_output_panel(PANEL_NAME)
        PANEL_VIEW.set_read_only(False)
        #panel_view.run_command("select_all") 
        #panel_view.run_command("right_delete")
        self.view.window().run_command("show_panel", {"panel": "output.{}".format(PANEL_NAME)})
        

        for p in PROCS:

            if str(p[3]) == ident:
                PANEL_VIEW.run_command("insert", {"characters": "LOG COMMAND: "+str(p[1])+" \nThread command: "+ str(p[3])+"\n"+p[4]})
                break
        PANEL_VIEW.set_read_only(True)
        LAST_SHOWED_CMD = p[3]



#class checkprocs(sublime_plugin.TextCommand):
#    def run(self, edit):
#
#        ap = self.view.window().active_panel()
#        print(ap)
#        #self.view.window().run_command("hide_panel", {"panel": "output.mypanel"})
#        self.view.window().run_command("show_panel", {"panel": "output.mypanel"})
#
#        global PROCS
#        alive_procs = []
#        for proc in PROCS:
#            if proc.is_alive():
#                alive_procs.append(proc)
#
#        PROCS = alive_procs
#        sublime.message_dialog(str(PROCS))  








#if len(self.view.sel())
#    sublime.error_message("Vous devez selectioner une seul region !")
#    exit(0)

#query = self.view.substr(self.view.sel()[0])

#self.view.replace(edit, self.view.sel()[0], result)
    

#    {
#        "caption": "Actions",
#        "id": "help2",
#        "children":
#        [
#            { "command": "remote_cmd", "args": {"type_cmd": 1 }, "caption": "Hive -e" },
#            { "command": "remote_cmd", "args": {"type_cmd": 2 }, "caption": "Show create table" },
#            { "command": "remote_cmd", "args": {"type_cmd": 3 }, "caption": "Limit 10" },
#            { "command": "remote_cmd", "args": {"type_cmd": 4 }, "caption": "Hdfs dfs -ls" },
#            { "command": "remote_cmd", "args": {"type_cmd": 5 }, "caption": "CMD" },
#        ]
#    }


